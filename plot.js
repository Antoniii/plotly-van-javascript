var data = [
  {
    x: [1, 2, 3, 4],
    y: [2, 1, 3, 4],
    error_y: {
      type: 'data',
      symmetric: false,
      array: [0.1, 0.2, 0.1, 0.1],
      arrayminus: [0.25, 0.74, 0.21, 0.2]
    },
    type: 'scatter'
  }
];
Plotly.newPlot('myDiv', data);

// https://plotly.com/javascript/
// https://plotly.com/javascript/error-bars/

// https://codepen.io/pen/